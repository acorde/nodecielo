//Gerencia as requisições, rotas, urls, etç
const express = require("express");
//Incluir as controllers
const customers = require("./controllers/customers");
//Chama a função express
const app = express();
app.use(express.json());
const db = require("./db/models");
//Criar as rotas
app.use("/", customers);
//Iniciar o servidor
app.listen(8082, () => {
    console.log("Servidor iniciado na porta 8081: http://localhost:8082");
});