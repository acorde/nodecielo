const express = require("express");
const router = express.Router();
const db = require('./../db/models');
const Card = require('./../db/models/Card');

async function gerarCartao(dadosUsuario){ 
    return new Promise((resolve, reject) => {
        Card.NumberCard = parseInt(Math.random() * 9999999999999999).toString();
        Card.ExpirationDate = "2024-10-13 11:25:28";
        Card.NameCard = dadosUsuario.NameBusiness;
        Card.Band = "VISA";
        Card.SecurityCode = "784";
        Card.CustomersId = dadosUsuario.id;
        
        db.Card.create(Card).then((dadosCartao) => {                                     
                                        resolve(dadosCartao)
                                    }).catch((error) => {    
                                        reject(error)    
                                        console.log("Erro: ocorreu um erro ao gerar o cartão.", error)
                                    });
    });
};

router.post("/unit-customers", async (req, res) => {
    var _customers = req.body;     
    var _address = req.body.Address
    //console.log("Conteúdo da variável _address: ", _address)
    return res.json({
        mensagem: "Conteúdo da variável _address: ", _address,
    });
});

router.post("/customers", async (req, res) => {
    var _customers = req.body;     
    var _address = req.body.Address

    await db.Customers.create(_customers)
    .then(async (dadosUsuario) => {         
        _address.CustomersId = dadosUsuario.id;
         await db.Address.create(_address).then(async (dadosAddress) => { 
            var _Customers = (dadosUsuario).toJSON();       
            _Customers.Address = (dadosAddress).toJSON();            
            await gerarCartao(dadosUsuario).then(async (dadosCartao) => {
                _Customers.Card = (dadosCartao).toJSON();
                /*return ret = res.json({
                    Success: true,
                    Message: "Dados do cartão realizado com sucesso!",
                    Card: _Customers,                
                });*/
            }).catch( (error) => {                
                //console.log("Erro: Dados do cartão Não realizado! ");     
                return ret = res.json({
                    Success: false,
                    Message: "Erro: Cadastrado Não realizado!! " + error,
                    Customers: _Customers, 
                });                               
            });
            return ret = res.json({
                Success: true,
                Message: "Cadastrado realizado com sucesso!",
                Customers: _Customers,                
            });
        });
        
    }).catch((error) => {
        return res.json({
            Success: false,
            Message: "Erro: Cadastrado Não realizado! " + error,
        });
    });    
});

router.get("/customers", async (req, res) => {
    const { page = 1 } = req.query;									//Calcular paginação.
		const limit = 10;                                               //Calcular paginação.
		var lastPage = 1;                                               //Calcular paginação.
		const countUser = await db.Customer.count();                       //Calcular paginação.
		if(countUser !== 0){                                            //Calcular paginação.
			lastPage = Math.ceil(countUser / limit);                    //Calcular paginação.
		}else{                                                          //Calcular paginação.
			return res.status(400).json({                               //Calcular paginação.
				mensagem: "Erro: Nenhum customers encontrado!"            //Calcular paginação.
			});                                                         //Calcular paginação.
		}                                                               //Calcular paginação.
    //
    const customers = await db.Customer.findAll({
        attributes: ['id','CodeHtml','CodeInternal','CnpjParameter','CNPJConsulted','NumberRegistration','NameBusiness','RegistrationStage','SupplierName'],
        order: [['id','DESC']],
        //Calcular a partir de qual registro deve retornar 
        //e o limite de registros
        offset: Number((page * limit) - limit),						//Calcular paginação
        limit: limit                                                //Calcular paginação
    });
    //

    if(customers){
        var pagination = {											//Retornar dados paginados.
            path: "/customers",                                         //Retornar dados paginados.
            page: page,                                             //Retornar dados paginados.
            prev_page_url: page - 1 >= 1 ? page - 1 : page,         //Retornar dados paginados.
            next_page_url: Number(page) + Number(1) > lastPage      //Retornar dados paginados.
                            ? lastPage : Number(page) + Number(1),  //Retornar dados paginados.
            lastPage: lastPage,                                     //Retornar dados paginados.
            total: countUser                                        //Retornar dados paginados.
        }                                                           //Retornar dados paginados.
        return res.json({
            users,
            pagination 												//Retornar dados paginados.
        });
    }else{
        return res.status(400).json({
            mensagem: "Erro: Nenhum usuário encontrado!"
        });
    }

});

module.exports = router;