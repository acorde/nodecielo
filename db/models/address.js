'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Address extends Model {
    /**
     * Address has one Customers (belongsTo) 1:1
     * Customers has many Cards (hasMany) 1:n
     * Employe has many WorkingDay and WorkingDay has many employees (manyToMany) n:n
     */
    static associate(models) {
      Address.belongsTo(models.Customers);
    }
  }
  Address.init({
    UfParameter: DataTypes.STRING,
    Logradouro: DataTypes.STRING,
    Numero: DataTypes.STRING,
    Complemento: DataTypes.STRING,
    CEP: DataTypes.STRING,
    Bairro: DataTypes.STRING,
    Municipio: DataTypes.STRING,
    Uf: DataTypes.STRING,
    CodeIbge: DataTypes.STRING,
    CustomersId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Address',
    timestamps: false,
    freezeTableName: true,
  });
  return Address;
};