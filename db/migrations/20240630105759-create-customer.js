'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Customers', {
      Id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      CodeHtml: { type: Sequelize.STRING, allowNull: false },
      CodeInternal: { type: Sequelize.STRING, allowNull: false },
      CnpjParameter: { type: Sequelize.STRING, allowNull: false },
      CNPJConsulted: { type: Sequelize.STRING, allowNull: false },
      NumberRegistration: { type: Sequelize.STRING, allowNull: false },
      NameBusiness: { type: Sequelize.STRING, allowNull: false },
      RegistrationStage: { type: Sequelize.STRING, allowNull: true },
      SupplierName: { type: Sequelize.STRING, allowNull: false }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Customers');
  }
};