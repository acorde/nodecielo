'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Address', {
      Id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      UfParameter: { type: Sequelize.STRING },
      Logradouro: { type: Sequelize.STRING },
      Numero: { type: Sequelize.STRING },
      Complemento: { type: Sequelize.STRING },
      CEP: { type: Sequelize.STRING },
      Bairro: { type: Sequelize.STRING },
      Municipio: { type: Sequelize.STRING },
      Uf: { type: Sequelize.STRING },
      CodeIbge: { type: Sequelize.STRING },
      CustomersId: { type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Customers',
          key: 'Id'
        }
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Address');
  }
};