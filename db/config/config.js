require("dotenv").config();
module.exports = {
  "development": {
    "username": process.env.DB_USER_NAME,
    "password": process.env.DB_PWD,
    "database": process.env.DB_NAME_DB,
    "host": process.env.DB_HOST,
    "port":  process.env.DB_PORT,
    "dialect": process.env.DB_DIALECT,
    "logging": true
  },
  "test": {
    "username": process.env.DB_USER_NAME,
    "password": process.env.DB_PWD,
    "database": process.env.DB_NAME_DB,
    "host": process.env.DB_HOST,
    "port":  process.env.DB_PORT,
    "dialect": process.env.DB_DIALECT,
    "logging": true
  },
  "production": {
    "username": process.env.DB_USER_NAME,
    "password": process.env.DB_PWD,
    "database": process.env.DB_NAME_DB,
    "host": process.env.DB_HOST,
    "port":  process.env.DB_PORT,
    "dialect": process.env.DB_DIALECT,
    "logging": true
  }
}
